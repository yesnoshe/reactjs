import logo from './logo.svg';
import './App.css';
import SignIn from './Components/SignIn';
import SignUp from './Components/SignUp';
import StyledSignIn from './Components/StyledSignIn';
import StyledSignUp from './Components/StyledSignUp';


function App() {
  return (
    <>
    <h4 style={{marginLeft:'800px'}}>Using CSS Module</h4>
    <SignUp/>
    <SignIn/>
    <h4 style={{marginLeft:'750px'}}>Using styled-components</h4>
    <StyledSignUp/>
    <StyledSignIn/>
    </>
  );
}

export default App;
