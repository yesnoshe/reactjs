import React from "react";
import styles from './SignUp.module.css'

const SignUp = () => {
    return(
        <div className={styles.container}>
            <h3 className={styles.title}>Sign Up For an Account</h3>
            <form className={styles.form}>
                <input type="text" className={styles.input} placeholder="Username" name="username"/>
                <input type="email" className={styles.input} placeholder="Email address" name="email"/>
                <input type="password" className={styles.input} placeholder="Password" name="password"/>
                <input type="password" className={styles.input} placeholder="Confirm Password" name="password"/> 
                <button type="submit" className={styles.btn}>Sign Up</button>
            </form>
        </div>
    )
}
export default SignUp;