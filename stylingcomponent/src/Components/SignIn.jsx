import React from "react";
import styles from './SignIn.module.css';

const SignIn = () => {
    return(
        <div className={styles.container}>
            <h3 className={styles.title}>Welcome Back! Sign In </h3>
            <form className={styles.form}>
                <input type="text" className={styles.input} placeholder="Username"/>
                <input type="password" className={styles.input} placeholder="Password"/>
                <button type="submit" className={styles.btn}>Sign Up</button>
            </form>
        </div>
    )
}
export default SignIn;
