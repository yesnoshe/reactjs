import React from 'react';
import styled from 'styled-components';

// Styled SignIn Container
const SignInContainer = styled.div`
    max-width: 300px;
    margin: 10px auto;
    padding: 20px;
    background-color:aliceblue;
    border-radius: 5px;
`;

// Styled SignIn Title
const SignInTitle = styled.h3`
  margin-top: 0;
  text-align: center;
`;

// Styled SignIn Form
const SignInForm = styled.form`
  margin-top: 20px;
`;

// Styled SignIn Input
const SignInInput = styled.input`
  width: 100%;
  padding: 10px;
  margin-bottom: 10px;
  border: none;
  border-radius: 3px;
`;

// Styled SignIn Button
const SignInButton = styled.button`
    width: 100%;
    padding: 10px;
    background-color: #000;
    color: #fff;
    border: none;
    border-radius: 20px;
    cursor: pointer;
    margin: 5px auto;
`;

// SignIn Component
const StyledSignIn = () => {
  return (
    <SignInContainer>
      <SignInTitle>Welcome Back! Sign In</SignInTitle>
      <SignInForm>
        <SignInInput type="text" placeholder="Username" />
        <SignInInput type="password" placeholder="Password" />
        <SignInButton type="submit">Sign In</SignInButton>
      </SignInForm>
    </SignInContainer>
  );
};

export default StyledSignIn;