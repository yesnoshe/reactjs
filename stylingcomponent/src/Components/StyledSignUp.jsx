import React from 'react';
import styled from 'styled-components';

// Styled SignUp Container
const SignUpContainer = styled.div`
  max-width: 300px;
  margin: 0 auto;
  padding: 20px;
  background-color:aliceblue;
  border-radius: 5px;
`;

// Styled SignUp Title
const SignUpTitle = styled.h3`
  margin-top: 0;
  text-align: center;
`;

// Styled SignUp Form
const SignUpForm = styled.form`
  margin-top: 20px;
`;

// Styled SignUp Input
const SignUpInput = styled.input`
  width: 250px;
  padding: 10px;
  margin-bottom: 10px;
  border: none;
  border-radius: 3px;
`;

// Styled SignUp Button
const SignUpButton = styled.button`
  width: 100%;
  padding: 10px;
  background-color: #000000;
  color: #fff;
  border: none;
  border-radius: 20px;
  cursor: pointer;
  margin: 5px auto;
 
`;

// SignUp Component
const StyledSignUp = () => {
  return (
    <SignUpContainer>
      <SignUpTitle>Sign Up For an Account</SignUpTitle>
      <SignUpForm>
        <SignUpInput type="text" placeholder="Username" />
        <SignUpInput type="email" placeholder="Email" />
        <SignUpInput type="password" placeholder="Password" />
        <SignUpInput type="password" placeholder="Confirm Password" />
        <SignUpButton type="submit">Sign Up</SignUpButton>
      </SignUpForm>
    </SignUpContainer>
  );
};

export default StyledSignUp;
