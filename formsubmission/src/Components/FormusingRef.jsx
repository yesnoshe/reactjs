import React, {useRef} from "react";

const FormusingRef  = () => {

    const inputRef =useRef();
    const dropdownRef =useRef();

    const submitHandler = (e) => {
        e.preventDefault();

        console.log("Entered Name: ", inputRef.current.value);
        console.log("Selected Program: ", dropdownRef.current.value);


    }
    return(
        <>
        <form 
        onSubmit={submitHandler}
        style={{margin:'10px 700px'}}
        >
            <h5>Your name</h5>
            <input
             ref={inputRef} 
             type="text" 
             placeholder="Enter your name"
             style={{padding: '10px 10px',
                    borderRadius: '10px',
                    width: '25vh',
            }}  
             />
            <h5>Choose your program</h5>
            <select 
            ref={dropdownRef}
            style={{padding: '10px 20px',
                    borderRadius: '10px',
                    width: '30vh',
            }} 
            >
                <option>Bachelor of Science in IT</option>
                <option>Bachelor of Science in CS</option>
                <option>Digital Media</option>
            </select>
            <div style={{marginTop:'20px'}}>
            <button 
            type="submit" 
            style={{padding: '5px 10px',
                    backgroundColor: '#000000',
                    borderRadius: '20px',
                    color:'white',
                    width:'20vh',
                    height:'2.5rem',
                    marginLeft:'30px'
            }}>
            Submit
            </button>
            </div>
        </form>
        </>
    )
}
export default FormusingRef;