import React, {useState} from 'react';

export default function FormSubmission() {
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const[emailError, setEmailError] = useState(false)
    const[passwordError, setPasswordError] = useState(false)

    const handleSubmit = (event) =>{
        event.preventDefault();
        if (email === '') {
            setEmailError(true)
        };
        if (password === '') {
            setPasswordError(true)
        }; 
    };
    return( 
        <div>
            <center>
            <form onSubmit={handleSubmit}>
                <p style={{fontWeight: '500', color: emailError ? 'red':'black'}}>Your Email</p>

                <input
                type="email"
                style={{
                    padding: '10px',
                    white: '400px',
                    borderRadius: '10px',
                    backgroundColor: emailError ? 'rgba(255, 0, 10, 0.2)': 'white',
                    minWidth: '400px'
                }}
                value={email}
                onChange={e => setEmail(e.target.value)}/><br/>

    <p style={{fontWeight: '500', color: passwordError ? 'red':'black'}}>Your Password</p>

    <input
                type="password"
                style={{
                    padding: '10px',
                    white: '400px',
                    borderRadius: '10px',
                    backgroundColor: passwordError ? 'rgba(255, 0, 10, 0.2)': 'white',
                    minWidth: '400px'
                }}
                value={password}
                onChange={e => setPassword(e.target.value)}/><br/>

                <button
                style={{
                    marginTop: '10px',
                    paddingInline: '40px',
                    paddingBottom: '5px',
                    backgroundColor: 'purple',
                    color: '#ffff',
                    minWidth: '200px',
                    minHeight: '50px',
                    borderRadius: '10px',
                    fontSize: '20px'
                    

                }}>
                    Submit</button>

            </form>
            </center>
        </div>
    )
}