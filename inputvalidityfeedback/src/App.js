import logo from './logo.svg';
import './App.css';
import FormSubmission from './FormSubmission';

function App() {
  return (
    <div className="App">
      <FormSubmission/>
    </div>
  );
}

export default App;
