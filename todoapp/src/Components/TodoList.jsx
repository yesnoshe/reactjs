import React from "react";
import TodoItem from "./TodoItem";

const TodoList = ({ todos, filter, onToggle, onClearCompleted }) => {
  // Filter todos based on filter value
  const filteredTodos = filter === "completed" ? todos.filter(todo => todo.completed) :
    filter === "uncompleted" ? todos.filter(todo => !todo.completed) :
    todos;

  return (
    <div className="todo-list">
      {filteredTodos.map(todo => (
        <TodoItem
          key={todo.id}
          text={todo.text}
          id={todo.id}
          completed={todo.completed}
          onClick={onToggle}
        />
      ))}
      {filter === "completed" && (
        <button className="clear-completed" onClick={onClearCompleted}>
          Clear Completed
        </button>
      )}
    </div>
  );
};

export default TodoList;
