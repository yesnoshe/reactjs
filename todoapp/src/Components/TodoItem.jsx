import React from "react";

const TodoItem = ({ text, id, completed, onClick }) => {
  const handleClick = () => {
    onClick(id); // Call onClick callback with todo item ID
  };

  return (
    <div
      className={`todo-item ${completed ? "completed" : ""}`}
      onClick={handleClick}
      style={{marginLeft: '750px'}}
    >
      {text}
    </div>
  );
};

export default TodoItem;
