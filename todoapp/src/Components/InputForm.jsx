import React, { useState } from "react";

const InputForm = ({ onAddTodo }) => {
  const [newTodo, setNewTodo] = useState(""); // Initialize todo item state

  const handleSubmit = (event) => {
    event.preventDefault(); // Prevent form submission
    if (newTodo.trim() !== "") {
      // Check if todo item is not empty
      onAddTodo(newTodo); // Call onAddTodo callback with new todo item
      setNewTodo(""); // Reset todo item state
    }
  };

  return (
    <form 
    onSubmit={handleSubmit}
    style={{margin: '20px 700px', padding: '20px', justifyContent: 'center'}}
    >
        <h3>To Do Application</h3>
      <input
        type="text"
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
        placeholder="Add a new todo..."
        style={{padding: '20px 20px', width: '50vh'}}
      />
      <button
       type="submit"
       style={{padding:'10px 10px',
                marginTop: '15px',
                width: '20vh',
                height: '3rem',
                backgroundColor:'#0099FA',
                borderBlockColor:'#0099FA',
                borderRadius: '15px',
                marginLeft: '150px'
            }}
        >Add Todo</button>
    </form>
  );
};

export default InputForm;
