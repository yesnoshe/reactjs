import React, { useState } from "react";
import InputForm from "./Components/InputForm";
import TodoList from "./Components/TodoList";

const App = () => {
  const [todos, setTodos] = useState([]);

  // Function to add a new todo item to the list
  const handleAddTodo = (text) => {
    const newTodo = {
      id: new Date().getTime(),
      text,
      completed: false
    };
    setTodos(prevTodos => [...prevTodos, newTodo]);
  };

  // Function to toggle the completion status of a todo item
  const handleToggleTodo = (id) => {
    setTodos(prevTodos => prevTodos.map(todo => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    }));
  };

  // Function to clear all completed todo items from the list
  const handleClearCompleted = () => {
    setTodos(prevTodos => prevTodos.filter(todo => !todo.completed));
  };

  return (
    <div className="app">
      <InputForm onAddTodo={handleAddTodo} />
      <TodoList
        todos={todos}
        onToggle={handleToggleTodo}
        onClearCompleted={handleClearCompleted}
      />
    </div>
  );
};

export default App;
